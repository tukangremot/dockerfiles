FROM php:7.4
RUN apt-get update \
    && apt-get -y install git zip libmcrypt-dev libpq-dev \
    && pecl install mcrypt \
    && pecl install mongodb \ 
    && pecl install xdebug \
    && docker-php-ext-enable mcrypt \
    && docker-php-ext-enable mongodb \
    && docker-php-ext-enable xdebug \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && echo xdebug.mode=coverage > /usr/local/etc/php/conf.d/xdebug.ini
